#!/usr/bin/python
# -*- coding: utf-8 -*-
# FireCracker WarGame Scoreboard
# AddFlag.py
# Jordan Brown 31/October/2015

import os
from boto import dynamodb2
from boto.dynamodb2.table import Table
import simplejson
import uuid
import hashlib
TABLE_NAME = "3441Challenges"
REGION = "ap-northeast-1"



conn = dynamodb2.connect_to_region(
    REGION,
    aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY']
)

# flag = raw_input("Enter Flag:")
salts = os.environ['ChallengeSalts'].split('+')

# #make a hash of it with the salt
# hashedFlag = flag
# for salt in salts:
# 	sha1 = hashlib.sha1()
# 	sha1.update(hashedFlag + salt)
# 	hashedFlag = sha1.hexdigest()
# print hashedFlag

# data = 	{
#     "flag": hashedFlag,
# 	"week": 1,
# 	"category": "TEST",
# 	"description": "This flag is '" + flag + "'",
# 	"format": "TEST{...}",
# 	"name": "Test",
# 	"points": 100
# }

# flagTable = Table(
#     TABLE_NAME,
#     connection=conn
# )


# flagTable.put_item(data)



for w in range(0,12):
	for i in range(0,3):
		flag = "TEST{week" + str(w) + "_test" + str(i) + "}"
		salts = os.environ['ChallengeSalts'].split('+')

		#make a hash of it with the salt
		hashedFlag = flag
		for salt in salts:
			sha1 = hashlib.sha1()
			sha1.update(hashedFlag + salt)
			hashedFlag = sha1.hexdigest()
		print hashedFlag

		data = 	{
		    "flag": hashedFlag,
			"week": w,
			"category": "TEST",
			"description": "The flag is " + flag,
			"format": "TEST{...}",
			"name": "TEST_" + str(w) + "_" + str(i),
			"points": 100
		}

		flagTable = Table(
		    TABLE_NAME,
		    connection=conn
		)


		flagTable.put_item(data)


# types = ["Easy", "Relevant","Advanced"]
# for t in range(3):

# 	for i in range(0,12):
# 		flag = "week" + str(i) + types[t]
# 		salts = os.environ['ChallengeSalts'].split('+')

# 		#make a hash of it with the salt
# 		hashedFlag = flag
# 		for salt in salts:
# 			sha1 = hashlib.sha1()
# 			sha1.update(hashedFlag + salt)
# 			hashedFlag = sha1.hexdigest()
# 		print hashedFlag

# 		data = 	{
# 		    "flag": hashedFlag,
# 			"week": i,
# 			"category": types[t],
# 			"description": "lorim ipsuma asidif lipodus",
# 			"format": "COMP3441{...}",
# 			"name": flag,
# 			"points": 100 * (t+1),
# 			"port" : 3441,
# 			"link" : "www.google.com"
# 		}

# 		flagTable = Table(
# 		    TABLE_NAME,
# 		    connection=conn
# 		)


# 		flagTable.put_item(data)



