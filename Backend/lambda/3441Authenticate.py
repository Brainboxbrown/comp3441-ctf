from __future__ import print_function

import json
import time
import boto3
import random
import base64
import hashlib

print('Loading function')
client = boto3.client('dynamodb')

def generate_token(user_id, timestamp, username=""):
	'''
	Generate a token based on user_id + timestamp + username + HMAC
	username is only used when a user is setting their username
	'''
	
	if username == None:
	    username = ""

	# This is my secret key. Shhhh don't tell the NSA
	key = ""

	blocksize = hashlib.sha1().block_size
	while len(key) < blocksize:
		key += '\x00'

	msg = str(user_id) +":"+ str(timestamp) +":"+ str(username) 

	key_list = list(key)
	
	o_key_list = []
	i_key_list = []

	for x in range(blocksize):
		o_key_list.append(str(0x5C ^ ord(key_list[x])))
		i_key_list.append(str(0x36 ^ ord(key_list[x])))


	o_key_pad = ''.join(o_key_list)
	i_key_pad = ''.join(i_key_list)

	h1 = hashlib.sha1(i_key_pad + msg).hexdigest()
	hmac = hashlib.sha1(o_key_pad + h1).hexdigest()


	return hmac

def get_user(user_id):
    table_name = "3441_ID"
    # try:
    response = client.get_item(TableName=table_name, Key={'user_id':{'S':user_id}})
    print ("the response")
    print (response)
    if len(response) > 0:
        return response
    # except:
        # pass
    # if there is a connection issue then we should treat it differently
    
    return None
    
    
    
def valid_timestamp(timestamp):
    return True
    # Get the current time
    # if the timestamp has expired return false
    current_time = int(time.time())
    if timestamp + 100 > current_time:
        return False
    return True
    
    
def valid_signature(user_id, timestamp, username, hmac):
    '''
    Check that the hmac is valid
    '''
    my_hmac = generate_token(user_id, timestamp, username)
    
    if my_hmac != hmac:
        return False
    return True
    
    


def add_user(user_id, username):
    '''
    Add a new user and return a session token
    '''
    table_name = "3441_ID"
    current_time = int(time.time())
    my_hmac = generate_token(user_id, current_time)
    
    session_token = str(user_id) +":"+ str(current_time) +":"+ str(my_hmac)
    
    
    
    try:
        client.update_item(TableName=table_name,
            Key = { 
                'user_id': { 
                    'S' : user_id
                    }
                },
            AttributeUpdates = {
                "username" : { "Value":{ 'S' : username }, "Action" : "PUT" },
                "session_token" : { "Value":{ 'S' : session_token }, "Action" : "PUT" },
                "flags" : { "Value":{ 'L' : [] }, "Action" : "PUT" },
                "score" : { "Value":{ 'N' : "0" }, "Action" : "PUT" },
                "level" : { "Value":{ 'N' : "0" }, "Action" : "PUT" }
            }
        ) 
        return session_token
    except Exception, e:
        print (e)
        return None
    
def get_session_token(user_id):
    '''
    Given a user_id return a new session token
    '''
    table_name = "3441_ID"
    # select the user_id from the table
    # generate a random thing and put it 
    current_time = int(time.time())
    my_hmac = generate_token(user_id, current_time)
    
    session_token = str(user_id) +":"+ str(current_time) +":"+ str(my_hmac)
    
    # try:
    client.update_item(TableName=table_name,
        Key = { 
            'user_id': { 
                'S' : user_id
                }
            },
            
        AttributeUpdates = {
            "session_token" : { "Value":{ 'S' : session_token }, "Action" : "PUT" },
        }
    ) 
    
    return session_token
    # except:
    #     pass
    return None



def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))
    
    if "user_id" in event:
        user_id = event['user_id']
    else:
        return "no user_id in request"
        
    if "timestamp" in event:
        timestamp = event['timestamp']
    else:
        return "no timestamp in request"
        
    if "hmac" in event:
        hmac = event['hmac']
    else:
        return "no hmac in request"
        
        
    username = None
    if 'username' in event:
        username = event['username']
    
    if not valid_timestamp(timestamp):
        return "Message has expired"
    
    if not valid_signature(user_id, timestamp, username, hmac):
        return "Invalid Signature"
    
    # At this point we have a valid signature
    # and it hasn't expired
    # so we'er cool to go ahead 
    
    user = get_user(user_id)
    
    print (user)
    print (username)
    
    if not "Item" in user:
        # this is the first time they are using the service
        
        if username == None:
            # If they didn't give a username, ask for one
            # get them to comeback with a username
            return "User not found"
        
        # so they gave us a username this time
        session_token = add_user(user_id, username)
        
        # TODO: less hacks
        return {"session_token": session_token}
    
    # The user already exists, send them a token
    session_token = get_session_token(user_id)
    if session_token == None:
        return "Error talking to backend things"
        
    print ( user )
    obj = user["Item"]
    username = ""
    level = ""
    score = ""
    flags = ""
    try:
        username = obj["username"]["S"]
        level = obj["level"]["N"]
        score = obj["score"]["N"]
        flags = obj["flags"]["L"]
    except:
        pass

        
    return {
        "session_token": session_token,
        "username" : username,
        "flags" : flags,
        "score" : score,
        "level" : level
    }
    
    
    
    
    
    
    