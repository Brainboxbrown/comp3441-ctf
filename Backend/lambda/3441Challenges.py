from __future__ import print_function

import json
import time
import boto3
import random
import base64
import hashlib

print('Loading function')
client = boto3.client('dynamodb')


# No auth for getting challenges
def lambda_handler(event, context):
    
    tableName = "3441Challenges";
    
    currentWeek = 1;
    
    table = client.scan(TableName=tableName)
    challenges = table["Items"]
    weeks = {}
    
    for challenge in challenges:
        # Remove the flag first just to be safe
        del challenge["flag"]
        
        challengeWeek = int(challenge["week"]["N"])
        
        # Remove the challenges for later weeks
        if challengeWeek > currentWeek:
            continue
        
        week_index = "week" + str(challengeWeek)
        if not week_index in weeks:
            weeks[week_index] = []
            
        weeks[week_index].append(challenge) 
        
        
    return weeks
    
    