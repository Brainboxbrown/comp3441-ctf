var AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB();
var flagSalts = [""];
var crypto = require('crypto');

//TODO: make this python and add signature checks
exports.handler = function(event, context) {
    console.log("Request received:\n", JSON.stringify(event));
    console.log("Context received:\n", JSON.stringify(context));
    console.log(Math.floor(new Date() / 1000));
    var tableName = "3441Challenges";
    var currentWeek = 1;
    var hashedFlag = event.flag;
    for (var salt in flagSalts){
        hashedFlag = crypto.createHash('sha1').update(hashedFlag + flagSalts[salt]).digest('hex');
    }
    var user_id = '';
    var auth = false;
    if (typeof event.user_id != 'undefined' && event.user_id.length > 0){
        auth = true;
        user_id = event.user_id;
    }
    
    dynamodb.getItem({
    "Key":{"flag" :{'S' : hashedFlag}},
    "TableName": tableName
    }, function(err1, flagData) {
        
        console.log('flag:'+JSON.stringify(flagData, null, '  '));
        if (err1 ||
        typeof flagData == 'undefined' ||
        typeof flagData.Item == 'undefined' || 
        typeof flagData.Item.flag == 'undefined' ||
        flagData.Item.flag.length <= 0) {
            context.fail('Failed to retrieve flag');
        } else {
            
            //This is a valid flag
            //If they are unauthenticated then just give the value of the flag
            if (!auth){
                context.succeed({"N":flagData.Item.points.N});
            }else if(flagData.Item.week.N < currentWeek){
                context.succeed({"N":flagData.Item.points.N, "S": "wrong week"});
            }else if(flagData.Item.week.N > currentWeek){
                context.fail('Failed to retrieve flag');
            }else {
                
                
                
                
                //They have given a user_id so now we
                //check that this user exists and then update their data
                dynamodb.getItem({
                "Key":{"user_id" :{'S' : user_id}},
                "TableName": "3441_ID" //The table with the user_ids
                }, function(err2, data2) {
                    if (err2) {
                        context.fail('Failed to find user');
                    } else {
                        
                        //The user exists and they've given a valid flag
                        //Put it in their inventory of flags
                        //if they alreadyhave this flag then nothing changes
                        
                        //Calcualte their current score
                        var oldScore = 0;
                        var alreadyOwned = false;
                        
                        
                        delete flagData.Item.flag;
                        
                        if (typeof data2.Item.flags != 'undefined'){
                            
                            //check if they already have the flag 
                            for (var flag in data2.Item.flags.L){
                                if (data2.Item.flags.L[flag].M.name.S === flagData.Item.name.S){
                                    //this is a flag you already have
                                    if (alreadyOwned){
                                        context.fail("flag exists twice in db");
                                    }
                                    console.log("They already own this flag. Updating");
                                    data2.Item.flags.L[flag].M.category.S = flagData.Item.category.S;
                                    data2.Item.flags.L[flag].M.points.N = flagData.Item.points.N;
                                    alreadyOwned = true;
                                }
                                oldScore += data2.Item.flags.L[flag].M.points.N*1;
                            }
                        }else{
                            //This is their first flag
                            //make a new array with the new flag
                            console.log('this is a fist flag for someone');
                            data2.Item.flags  = {"L": []};
                        }
                        
                        if (!alreadyOwned){
                            //They don't have this flag already
                            flagData.Item.timestamp = {"N": Math.floor(new Date() / 1000).toString()};
                            data2.Item.flags.L.push({"M":flagData.Item});
                        }
                        
                        
                        //Calculate their new score
                        var newScore = 0;
                        for (var capFlag in data2.Item.flags.L){
                            newScore += data2.Item.flags.L[capFlag].M.points.N*1;
                        }
                        
                        var potentialPoints = flagData.Item.points.N*1;
                        var gainedPoints = newScore - oldScore;
                        
                        //calculate the level
                        var level =  (newScore - newScore%100)/100;
                        data2.Item.score = {"N" : newScore.toString()};
                        data2.Item.level = {"N" : level.toString()};
                        
                        // data2.Item.timestamp = {"N": "0"};
                        //This is their all-time timestamp
                        if (!alreadyOwned){
                            data2.Item.timestamp = {"N": Math.floor(new Date() / 1000).toString()};
                        }
                        console.log('Current situation:'+JSON.stringify(data2, null, '  '));
                        
                        
                        //Finaly update the user's flag data
                        dynamodb.putItem({
                            "TableName": "3441_ID",
                            "Item": data2.Item
                        }, function(err3, data3) {
                            if (err3) {
                                console.log('so this is where we meet ' + err3);
                                context.done('error','putting item into dynamodb failed: '+err3);
                            }
                            else {
                                
                                //What we want is the total score, points earned, potential points, level,
                                var returnResults = {
                                    "score" : {"N":newScore},
                                    "pointsEarned": {"N":gainedPoints},
                                    "potentialPoints": {"N":potentialPoints},
                                    "level": {"N":level},
                                    "alreadyOwned": {"Bool":alreadyOwned},
                                    "username" : {"S": data2.Item.username.S}
                                };
                                context.succeed(returnResults);
                            }
                        });
                
                    }
                });
            }
        }
    });
}