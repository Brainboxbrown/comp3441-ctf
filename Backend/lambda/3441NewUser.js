

console.log('Loading event');
var AWS = require('aws-sdk');
// var request = require('request');
var dynamodb = new AWS.DynamoDB();
var captchaStatus = "undefined";

// We need this to build our post string
var querystring = require('querystring');
var https = require('https');
var fs = require('fs');


exports.handler = function(event, context) {
	console.log("Request received:\n", JSON.stringify(event));
	console.log("Context received:\n", JSON.stringify(context));
	var post_options = {};
	var g_recaptcha_response = '';

	if (event.hasOwnProperty("g_recaptcha_response")){
		g_recaptcha_response = event.g_recaptcha_response;
		delete event.g_recaptcha_response;
	}else{
		context.fail('No captcha');
		return
	}

	// Get the username and password out of the event object
	var userData = extract_data(event);

	safeName = userData.username.replace(/[^\d.a-zA-Z!@#$~]+/g, '');
	safePass = userData.password.replace(/[^\d.a-zA-Z!@#$~]+/g, '');

	if (safeName != userData.username){
		context.fail('Hacking Detected 0');
		return
	}
	
	if (safePass != userData.password){
		context.fail('Hacking Detected 0');
		return
	}

	//If there is anything else in the event thing then we have problems
	if (!isEmpty(event)){
		context.fail('Hacking Detected 1');
		return
	}

	post_options = get_post_options(g_recaptcha_response);
	post_data = get_post_data(g_recaptcha_response);

	// Set up the request
	var post_req = https.request(post_options, function(res) {
		res.setEncoding('utf8');
		res.on('data', function (chunk) {

			//SUCCESS!!!!
			if (JSON.parse(chunk).success){
				dealWithIt(userData, context);
			} else {
				captchaStatus = "fail";
				context.fail('Hacking Detected 2');
			}
		});
	});


	// post the data
	post_req.write(post_data);
	post_req.end();


};

function get_post_data(g_recaptcha_response){
	// Build the post string from an object
	var post_data = querystring.stringify(
		{
			'secret' : "",
			'response' : g_recaptcha_response
		}
	);
	return post_data;
}

function get_post_options(g_recaptcha_response){
	var post_data = get_post_data(g_recaptcha_response);

	// An object of options to indicate where to post to
	post_options = {
		host: 'google.com',
		port: '443',
		path: '/recaptcha/api/siteverify',
		method: 'POST',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': Buffer.byteLength(post_data)
		}
	};

	return post_options;
}

function extract_data(event){
	var userData = {};
	if (event.hasOwnProperty("username")){
		userData.username = event.username;
		delete event.username;
	}
	if (event.hasOwnProperty("password")){
		userData.password = event.password;
		delete event.password;
	}

	return userData;
}

function isEmpty(obj) {
  return Object.keys(obj).length === 0;
}

function dealWithIt(userData, context){
	var tableName = "3441Users";
	var username = userData.username;
	dynamodb.getItem(
	{
		"Key": 
		{
			"username" :{'S' : username}
		},
		"TableName": tableName
	}, function(err1, data1) {
		if (err1) {
			context.fail('Umm shitttt');
		} else {
			if (typeof data1.Item != 'undefined'){
				context.fail('User exists');
			}else{

				var applicationSalt = "";
				var crypto = require('crypto');
				var token = crypto.randomBytes(64).toString('hex');



				//If there is no user allready ***
				//then add the user ***
				
				var password = userData.password;
				
				password = crypto.createHash('sha1').update(password + applicationSalt + token).digest('hex');
				
				dynamodb.putItem(
				{
					"TableName": tableName,
					"Item":{
						"username":{"S":username},
						"password":{"S":password},
						"token":{"S":token}
					}
				}, function(err2, data2) {
					if (err2) {
						context.done('error','putting item into dynamodb failed: '+err2);
					} else {
						console.log('great success: '+JSON.stringify(data2, null, '  '));

						//Finaly put the token in the main database
						dynamodb.putItem(
						{
							"TableName": "3441Table",
							"Item":{
								"token":{"S":token},
								"username":{"S":username},
								"score":{"N":"0"},
								"level":{"N":'0'}
							}
						}, function(err3, data3) {
							if (err3) {
								context.done('error','putting item into dynamodb failed: '+err3);
							}
							else {
								console.log('great success: '+JSON.stringify(data3, null, '  '));
								context.succeed(
									{
										"S": token
									}
								);
							}
						});
					}
				});
			}
		}
	});
}	


/*



console.log('Loading event');
var AWS = require('aws-sdk');
// var request = require('request');
var dynamodb = new AWS.DynamoDB();
var captchaStatus = "undefined";

// We need this to build our post string
var querystring = require('querystring');
var https = require('https');
var fs = require('fs');
function PostCode(g_recaptcha_response) {
  // Build the post string from an object
  var post_data = querystring.stringify({
      'secret' : "",
      'response' : g_recaptcha_response
  });

  // An object of options to indicate where to post to
  var post_options = {
      host: 'google.com',
      port: '443',
      path: '/recaptcha/api/siteverify',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data)
      }
  };

  // Set up the request
  var post_req = https.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + chunk);
          if (chunk.success){
              captchaStatus = "success";
          } else {
              captchaStatus = "fail";
          }
      });
  });

  // post the data
  post_req.write(post_data);
  post_req.end();

}

exports.handler = function(event, context) {
    console.log("Request received:\n", JSON.stringify(event));
    console.log("Context received:\n", JSON.stringify(context));
    
    
     // Build the post string from an object
  var post_data = querystring.stringify({
      'secret' : "",
      'response' : event.g_recaptcha_response
  });

  // An object of options to indicate where to post to
  var post_options = {
      host: 'google.com',
      port: '443',
      path: '/recaptcha/api/siteverify',
      method: 'POST',
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(post_data)
      }
  };

  // Set up the request
  var post_req = https.request(post_options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {
          console.log('Response: ' + JSON.stringify(chunk));
          console.log('Response: ' + JSON.parse(chunk));
          if (JSON.parse(chunk).success){
              captchaStatus = "success";
              
    var tableName = "3441Users";
    var username = event.username;
     dynamodb.getItem({
    "Key": 
        {
            "username" :{'S' : username}
        },
    "TableName": tableName
    }, function(err1, data1) {
        if (err1) {
            context.fail('Umm shitttt');
        } else {
            if (typeof data1.Item == 'undefined'){
                
                
                var applicationSalt = "";
                var crypto = require('crypto');
                var token = crypto.randomBytes(64).toString('hex');
                
                
                
                //If there is no user allready ***
                //then add the user ***
                
                var password = event.password;
                
                password = crypto.createHash('sha1').update(password + applicationSalt + token).digest('hex');
                
                dynamodb.putItem({
                     "TableName": tableName,
                        "Item":{
                            "username":{"S":username},
                            "password":{"S":password},
                            "token":{"S":token}
                    }
                }, function(err2, data2) {
                    if (err2) {
                        context.done('error','putting item into dynamodb failed: '+err2);
                    }
                    else {
                        console.log('great success: '+JSON.stringify(data2, null, '  '));
                        
                        //Finaly put the token in the main database
                        dynamodb.putItem({
                         "TableName": "3441Table",
                            "Item":{
                                "token":{"S":token},
                                "username":{"S":username},
                                "score":{"N":"0"},
                                "level":{"N":'0'}
                            }
                        }, function(err3, data3) {
                            if (err3) {
                                context.done('error','putting item into dynamodb failed: '+err3);
                            }
                            else {
                                console.log('great success: '+JSON.stringify(data3, null, '  '));
                                context.succeed({"S": token});
                            }
                        });
                    }
                });
            }else{
                context.fail('User exists');
            }
        }
    });
    
          } else {
                captchaStatus = "fail";
                context.fail('You fucked the captcha');
          }
      });
  });

  // post the data
  post_req.write(post_data);
  post_req.end();
    
    
};


function dealWithIt(userData){
    
}




*/
