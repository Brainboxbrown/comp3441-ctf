import json
import random
import hashlib
import requests


def generate_hmac(user_id, timestamp, username=""):
    '''
    Generate a token based on user_id + timestamp + username + HMAC
    username is only used when a user is setting their username
    '''

    if username == None:
        username = ""

    # This is my secret key. Shhhh don't tell the NSA
    key = ""

    blocksize = hashlib.sha1().block_size
    while len(key) < blocksize:
        key += '\x00'

    msg = str(user_id) +":"+ str(timestamp) +":"+ str(username) 

    key_list = list(key)
    o_key_list = []
    i_key_list = []

    for x in range(blocksize):
        o_key_list.append(str(0x5C ^ ord(key_list[x])))
        i_key_list.append(str(0x36 ^ ord(key_list[x])))


    o_key_pad = ''.join(o_key_list)
    i_key_pad = ''.join(i_key_list)

    h1 = hashlib.sha1(i_key_pad + msg).hexdigest()
    hmac = hashlib.sha1(o_key_pad + h1).hexdigest()


    return hmac

def webpage(session_token):
    '''
    Return the CTF in an iframe
    '''
    
    return '''
    <script src='https://www.openlearning.io/openlearning.js'></script>


    <script>
        OL(function() {{
            // Ready to go!
            OL.log('I'm ready!');
            OL.resize(1000, false); // fix the height of the widget to be 250px
            OL.resize(); // notify the parent that it needs to resize the widget
        }});

        OL.log('Out here');

        OL.notify('You are Awesome', 'Just letting you know...', 'success');

        OL.resize(1000, true); // fix the height of the widget to be 250px
        OL.resize(); // notify the parent that it needs to resize the widget

    </script>
    <iframe src='https://d2kiwew1poh2og.cloudfront.net/#/?session_token={0}' width='1000' height='1000'>
    '''.format(session_token)


def get_username(user_id, timestamp, hmac):
    return '''
            <b> Enter the username that you want: </b>
                <form action='https://1c98fb93.ngrok.io/new_user' method='post'>
                <dl>
                    <dt>Username:
                    <dd><input type='text' name='username'>
                    <dd><input type='hidden' name='user_id' value='{0}'>
                    <dd><input type='hidden' name='timestamp' value='{1}'>
                    <dd><input type='hidden' name='hmac' value='{2}'>
                    <dd><input type='submit' value='Login'>
                </dl>
            </form>
            '''.format(user_id, timestamp, hmac)


def lambda_handler(event, context):

    # get all the things from the post
    hmac = ""
    user_id = ""
    username = ""
    timestamp = ""
    
    parameters = ["hmac", "user_id", "timestamp", "username"]
    for parameter in parameters:
        if parameter not in event:
            return "{0} not in POST".format(parameter)
            
    hmac = event["hmac"]
    user_id = event["user_id"]
    username = event["username"]
    timestamp = event["timestamp"]
    
    my_hmac = generate_hmac(user_id, timestamp)

    if my_hmac != hmac:
        return "signature invalid. Hacking detected. Maybe."

    if not username.isalnum():
        return "Username must be alphanumeric. If you start using other characters it just breaks everything. <br>" + get_username(user_id, timestamp, hmac)



    # at this point we know that the only thing that an attacker could do
    # is to change their own username
    # but they had control over that anyway
    # this means that the filtering of usernames should be done here

    # generate a signature with the username too now, so that nothing can be changed
    signature = generate_hmac(user_id, timestamp, username)
    data = {
            "user_id" : user_id,
            "timestamp" : timestamp,
            "hmac" : signature,
            "username" : username
    }
    r = requests.post("https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/3441Authenticate", json=data)
    resp = r.json()


    session_token = resp['session_token']

    return webpage(session_token)



