#!/usr/bin/python
import json

import requests
from flask import Flask
from flask import request


class OAuthInvalidError(Exception):
     pass

app = Flask(__name__)

@app.route('/new_user', methods=['POST'])
def new_user():
    r = requests.post("https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/set_username", json=request.form)
    return str(r.text)

@app.route('/lti', methods=['POST'])
def lti_provider():
    r = requests.post("https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/lti", json=request.form)
    return str(r.text)

if __name__ == '__main__':
        app.run(host="0.0.0.0", port=1234, debug=True)


