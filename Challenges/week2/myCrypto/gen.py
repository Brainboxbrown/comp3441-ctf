#!/usr/bin/python
import sys

while True:
	string = raw_input('Plaintext:')
	words = string.split()

	maxLen = 0
	for word in words:
		if len(word) > maxLen:
			maxLen = len(word)


	print 'Ciphertext:',
	for x in range(0,maxLen):
		for word in words:
			try:
				sys.stdout.write(word[x])
			except:
				sys.stdout.write('_')
		print ' ',
	print
	print