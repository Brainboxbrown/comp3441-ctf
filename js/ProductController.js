app.controller('ProductController', function($scope, $resource, $location, $window, $cookies, $cookieStore, $uibModal, $log){
	
  /*
	* pointsStatus
	*******BLUE*******
	* 00 == login message

	*******GREEN*******
	* 10 == start hacking
	* 11 == your not logged in and this is right
	* 12 == you already own that flag
	* 13 == that's correct but the game is over
	* 14 == good work you get some points

	*******ORANGE*******
	* 20 == fuck off that flag isn't legit
	* 21 == your not logged in and this is wrong


	*******RED*******
	* 30 == wrong password/username

	####################
	## ADD 40 - MOD10 ## Add 30 to the status code and whatever 
	## GIVES LOAD CLR ## the first digit is determines the loading colour
	####################
	* 4X == loading blue
	* 5X == loading green
	* 6X == loading orange
	* 7X == loading red
	*
	*/


	var statusCodes = {
		"login" : 00,
		"start hacking" : 10,
		"no auth correct" : 11,
		"already owned" : 12,
		"correct over" : 13,
		"correct" : 14,
		"incorrect" : 20,
		"no auth incorrect" : 21,
		"wrong login" : 30,
		"loading blue" : 40,
		"loading green" : 50,
		"loading orange" : 60,
		"loading red" : 70
	};

	var getPass = $resource('https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/ctf/flag');
	var scoreboardResource = $resource('https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/ctf/scoreboard');
	var challengeResource = $resource('https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/ctf/challenges');
	var loadUserResource = $resource(' https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/auth/user');
	
	$scope.statusCodes = statusCodes;
	$scope.authenticated = true;
	$scope.pointsStatus = statusCodes["login"];
	$scope.pointsEarned = 0;
	$scope.potentialPoints = 0;
	$scope.score = 0;
	$scope.level = 0;
	$scope.tryPass = '';
	$scope.noAuthPoints = 0;
	$scope.rank = '_';
	$scope.screenWidth = window.innerWidth;

	$scope.numberOfWeeks = 12;
	$scope.weeks = [];
	for (var i  = 0; i <= $scope.numberOfWeeks; i++){
		$scope.weeks.push(i);
	}
	$scope.leaderboard = [];

	tokenValues = function () {

		var searchObject = $location.search();

		$scope.userToken  = ""

		if ("session_token" in searchObject){
			$scope.userToken  = searchObject['session_token']
		} else {
			$scope.userToken  = $cookies.get('pointlessToken') 
			if (!$scope.userToken || $scope.userToken  == '' || $scope.userToken  == 'VlROU2RtTkRRbk5pTWpseVlWYzFia2xIUmpCSlJ6RTFTVWRPZG1JeWRIQmFXRTFMQ2c9PQo=') {
				$scope.authenticated = false;
				return null;
			}
		}

		$cookies.put('pointlessToken', $scope.userToken);

		token = $cookies.get('pointlessToken') 
		console.log(token);

		values = token.split(':')

		return values;
	}

	//shouldn't need this whole 'first' thingo
	$scope.submitFlag = function() {

		values = tokenValues();

		user_id = values[0];
		timestamp = values[1];
		hmac = values[2];

		$scope.pointsStatus += 40 - ($scope.pointsStatus%10); //turns it into a loading animation


		getPass.save(
			{
				flag : $scope.tryPass,
				user_id : user_id,
				timestamp : timestamp,
				hmac: hmac
			}, function(response){

				if (response.score){
					
					$scope.score = response.score.N*1;
					$scope.pointsEarned = response.pointsEarned.N*1;
					$scope.potentialPoints = response.potentialPoints.N*1;
					$scope.pointsStatus = statusCodes["correct"];
					if (response.alreadyOwned.Bool){
						$scope.pointsStatus = statusCodes["already owned"];
					}
					$scope.level = response.level.N*1; // THis should be handled here
				}else if(response.S && response.S == "wrong week"){
					$scope.pointsStatus = statusCodes["correct over"]
				}else{
					$scope.pointsStatus = statusCodes["incorrect"];
				}
				updateScoreboard();
			}
		);

		// getPass.get({flag:$scope.tryPass, user_id:$cookies.get('pointlessToken').split(':')[0]})
		// .$promise.then(function(response){
		// 	if (response.score){
				
		// 		$scope.score = response.score.N*1;
		// 		$scope.pointsEarned = response.pointsEarned.N*1;
		// 		$scope.potentialPoints = response.potentialPoints.N*1;
		// 		$scope.pointsStatus = statusCodes["correct"];
		// 		if (response.alreadyOwned.Bool){
		// 			$scope.pointsStatus = statusCodes["already owned"];
		// 		}
		// 		$scope.level = response.level.N*1; // THis should be handled here
		// 	}else if(response.S && response.S == "wrong week"){
		// 		$scope.pointsStatus = statusCodes["correct over"]
		// 	}else{
		// 		$scope.pointsStatus = statusCodes["incorrect"];
		// 	}
		// 	updateScoreboard();
		// });
	};


	loadChallenges = function () {
		challengeResource.get().$promise.then(
			function(response){
				$scope.challenges = response;
				var curWeek = 0;
				while ($scope.challenges['week' + (curWeek + 1)]){
					curWeek ++;
				}
				$scope.currentWeek = curWeek;
				updateScoreboard();
			}
		);
	}

	getWeek = function(challenge){
		return 'week' + getWeekNum(challenge);
	}
	getWeekNum = function(challenge){
		for (var weekId in $scope.challenges){
			if (weekId.indexOf('week') == -1) continue;

			var week = $scope.challenges[weekId];
			for (var chall in week){
				if (week[chall].name.S == challenge){
					return weekId.substr(weekId.length - 1);
				}
			}
		}
		return '0';
	}

	resetChallenges = function() {

		for (var week in $scope.challenges){
			for (var challenge in $scope.challenges[week]) { //all, week1, week2 ...
				$scope.challenges[week][challenge]['solved'] = false;
			}
		}
	}




	/*
	 * This giant function gets the scoreboard from the lambda function
	 * then it goes through every user and updates them on the board
	 * finally it checks all the challenges and marks the ones you have solved
	 */

	updateScoreboard = function () {

		scoreboardResource.get().$promise.then(function(response){
			if (response.board){

				//Get all the things
				var board = response.board;


			
				//Initialise the boards
				$scope.leaderboard["all"] = [];
				for (var i = 0; i <= $scope.numberOfWeeks; i++){
					$scope.leaderboard["week" + i] = [];
				}
				

				//Split everything into the different leaderboards
				for (var userIndex in board){
					var user = board[userIndex];

					//Initialise each board at the index of the current user
					for (var fw in $scope.weeks){
						$scope.leaderboard["week" + fw][userIndex] = {};
						$scope.leaderboard["week" + fw][userIndex]['score'] = 0;
						$scope.leaderboard["week" + fw][userIndex]['level'] = user.level;
						$scope.leaderboard["week" + fw][userIndex]['username'] = user.username;
						$scope.leaderboard["week" + fw][userIndex]['timestamp'] = 0;
					}

					$scope.leaderboard["all"][userIndex] = {};
					$scope.leaderboard["all"][userIndex]['score'] = user.score;
					$scope.leaderboard["all"][userIndex]['level'] = user.level;
					$scope.leaderboard["all"][userIndex]['username'] = user.username;
					$scope.leaderboard["all"][userIndex]['timestamp'] = user.timestamp;


					if ($scope.authenticated && user['username'] == $scope.username){
						$scope.score = user['score'];
					}

					for (var flagIndex in user['solved']){
						//For every flag that a user has solved


						var flag = user['solved'][flagIndex];
						var flagWeek = getWeek(flag.name); //The week relating to the flag gained
						if (getWeekNum(flag.name) > $scope.currentWeek){
							continue;
						}
						//Points are per board but title is global
						$scope.leaderboard[flagWeek][userIndex]['score'] += 1*flag.points;
						//Make you're timestamp the largest timestamp from all your flags
						if ($scope.leaderboard[flagWeek][userIndex]['timestamp'] < flag['timestamp']){
							$scope.leaderboard[flagWeek][userIndex]['timestamp'] = flag['timestamp'];
						}


						// $scope.leaderboard["all"][userIndex]['score'] += 1*flag.points;

						if ($scope.authenticated && user['username'] == $scope.username){
							//This is you

							//For each challenge, mark it if you solved it
							for (chall in $scope.challenges[flagWeek]){
								// If this flag is the one that you have solved
								// mark it
								if ($scope.challenges[flagWeek][chall].name.S == flag.name){
									$scope.challenges[flagWeek][chall].solved = true;
								}
							}
						}
					}
				}


				for (var category in $scope.leaderboard) { //all, week1, week2 ...
					$scope.leaderboard[category].sort(function(a,b){
						return b['score'] - a['score'] || a['timestamp'] - b['timestamp'];
					});
				}

				if($scope.authenticated){
					for (var index in $scope.leaderboard["all"]){
						if ($scope.leaderboard["all"][index].username === $scope.username){
							//'all' will only show the leaderboard
							$scope.rank = index*1 + 1;
							console.log('adsfjklhadfjkhlfhjkl')
						}
					}
				}else{
					resetChallenges();
				}
			}else{
				//handle this better
				//can't load scoreboard
				console.log('Failed to load the scoreboard');
			}
		});
	}

	//Take the session_id and load their challenges and stuff. Get the username
	authenticate = function() {

		//Get the session token from the url

		values = tokenValues();

		if (values == null) {
			loadChallenges();
			updateScoreboard();
			return 
		}

		user_id = values[0];
		timestamp = values[1];
		hmac = values[2];

		$scope.pointsStatus += 40 - ($scope.pointsStatus%10); //turns it into a loading animation

		// Get the token from the url
		// Post to https://8pb7hlzksb.execute-api.ap-northeast-1.amazonaws.com/prod/3441Authenticate

		loadUserResource.save(
			{
				timestamp: timestamp,
				user_id: user_id,
				hmac: hmac
			}, function(response){

				if (typeof(response) != "undefined" && typeof(response.session_token) != "undefined" ){

					$scope.authenticated = true;
					$scope.username = response.username
					$scope.score = response.score.N;
					$scope.solvedFlags = response.flags // I'm getting to that
					$scope.pointsStatus = statusCodes["start hacking"];

				}else{
					$scope.pointsStatus = statusCodes["wrong login"];
					$scope.authenticated = false;
				}
				loadChallenges();
			}
		);

	};



	authenticate();
});






